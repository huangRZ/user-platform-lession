package org.example.annotation;

import java.lang.annotation.*;

/**
 * 请求的request body是JSON的标记
 * 当标记在请求方法上时，JSON会被反序列化成对象
 *
 * @author huangrz
 * @since 2021/3/3
 **/
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestBody {
}
