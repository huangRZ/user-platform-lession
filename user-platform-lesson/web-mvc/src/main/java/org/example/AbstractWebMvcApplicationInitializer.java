package org.example;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.IOException;

/**
 * 写点什么？
 *
 * @author hrz
 * @since 2021/3/3 0003
 */
public abstract class AbstractWebMvcApplicationInitializer implements WebMvcApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        try {
            WebMvcContext webMvcContext = new WebMvcContext();
            webMvcContext.init(servletContext, basePackage());
            // 将 MVC上下文 保存到 Servlet上下文中
            servletContext.setAttribute(WebMvcContext.CONTEXT_NAME, webMvcContext);
        } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            throw new ServletException("WebMvcContext 初始化失败！", e);
        }
    }

    public abstract String basePackage();
}
