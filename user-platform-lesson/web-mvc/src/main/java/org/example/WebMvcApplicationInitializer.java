package org.example;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * 写点什么？
 *
 * @author hrz
 * @since 2021/3/3 0003
 */
public interface WebMvcApplicationInitializer {

    void onStartup(ServletContext servletContext) throws ServletException;

}
