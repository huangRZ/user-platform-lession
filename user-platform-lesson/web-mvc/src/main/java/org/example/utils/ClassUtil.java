package org.example.utils;

import org.example.WebMvcContext;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * 写点什么？
 *
 * @author hrz
 * @since 2021/3/2 0002
 */
public class ClassUtil {


    /**
     * 获取同一路径下所有子类或接口实现类
     *
     * @param cls
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static List<Class<?>> getAllAssignedClass(Class<?> cls) throws IOException,
            ClassNotFoundException {
        List<Class<?>> classes = new ArrayList<>();
        for (Class<?> c : getClasses(cls)) {
            if (cls.isAssignableFrom(c) && !cls.equals(c)) {
                classes.add(c);
            }
        }
        return classes;
    }

    /**
     * 取得当前类路径下的所有类
     *
     * @param basePackage
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static List<Class<?>> getClasses(String basePackage) throws IOException,
            ClassNotFoundException {
        String path = basePackage.replace('.', '/');
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        URL url = classloader.getResource(path);
        if (url == null) {
            throw new IOException("path["+path+"]无法解析出URL！");
        }
        return getClasses(new File(url.getFile()), basePackage);
    }

    /**
     * 取得当前类路径下的所有类
     *
     * @param cls
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static List<Class<?>> getClasses(Class<?> cls) throws IOException,
            ClassNotFoundException {
        String pk = cls.getPackage().getName();
        return getClasses(pk);
    }

    /**
     * 迭代查找类
     * @param dir
     * @param pk
     * @return
     * @throws ClassNotFoundException
     */
    private static List<Class<?>> getClasses(File dir, String pk) throws ClassNotFoundException {
        List<Class<?>> classes = new ArrayList<Class<?>>();
        File[] files = null;
        if (!dir.exists() || (files = dir.listFiles()) == null) {
            return classes;
        }
        for (File f : files) {
            if (f.isDirectory()) {
                classes.addAll(getClasses(f, pk + "." + f.getName()));
            }
            String name = f.getName();
            if (name.endsWith(".class")) {
                classes.add(Class.forName(pk + "." + name.substring(0, name.length() - 6)));
            }
        }
        return classes;
    }


    public static void main(String[] args) {
        try {
            System.out.println("获取所有子类和实现类：");
            for (Class<?> c : getAllAssignedClass(WebMvcContext.class)) {
                System.out.println(c.getName());
            }
            System.out.println("获取所有类：");
            for (Class<?> c : getClasses(WebMvcContext.class)) {
                System.out.println(c.getName());
            }
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }




}