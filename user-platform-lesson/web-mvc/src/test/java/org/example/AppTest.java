package org.example;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        Class<WebMvcServletContainerInitializer> aClass = WebMvcServletContainerInitializer.class;
        System.out.println();
    }

    @Test
    public void testMethod() throws IllegalAccessException, InstantiationException, InvocationTargetException {
        Class<Cat> catClass = Cat.class;
        Cat cat = catClass.newInstance();
        for (Method declaredMethod : catClass.getDeclaredMethods()) {
            System.out.println("当前方法："+declaredMethod.getName());
            System.out.println("当前方法参数类型列表："+ Arrays.toString(declaredMethod.getParameterTypes()));
            System.out.println("当前方法参数名称列表："+ Arrays.toString(declaredMethod.getParameters()));
            System.out.println("执行结果："+declaredMethod.invoke(cat, "饼干", "水"));
        }
    }
}
