<head>
<jsp:directive.include
	file="/WEB-INF/jsp/prelude/include-head-meta.jspf" />
<title>My Home Page</title>
</head>
<body>
	<div class="container-lg">
		<!-- Content here -->
		<c:choose>
			<c:when test="${org.example.user != null}">Hello,World ${org.example.user.name}(${org.example.user.email})</c:when>
			<c:otherwise>用户未登录，<a href="login.jsp">去登陆>></a></c:otherwise>
		</c:choose>
	</div>
</body>