$(document).ready(function(){

    $("#register_button").click(function (){
        let inputEmail = $("#inputEmail").val();
        let inputPassword = $("#inputPassword").val();
        let userName = $("#userName").val();
        let phoneNumber = $("#phoneNumber").val();
        console.log("输入内容：", inputEmail, inputPassword, userName, phoneNumber)

        let user = {};
        user.email = inputEmail;
        user.password = inputPassword;
        user.name = userName;
        user.phoneNumber = phoneNumber;

        $.ajax({
            url: "/register",
            type: "POST",
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(user),
            success: function (result) {
                if (result.success) {
                    console.log("注册成功")
                    alert("注册成功")
                    window.location.href = "/login.jsp";
                } else {
                    console.log("注册失败");
                    alert("注册失败");
                }
            },
            error: function () {

            }
        });
    })

})
