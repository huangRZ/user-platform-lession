<head>
<%@ page contentType="text/html; charset=UTF-8" %>
<script src="/static/js/register.js" crossorigin="anonymous"></script>
<jsp:directive.include file="/WEB-INF/jsp/prelude/include-head-meta.jspf" />
	<title>My Home Page</title>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-org.example.user-select: none;
        -moz-org.example.user-select: none;
        -ms-org.example.user-select: none;
        org.example.user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
</head>
<body>
	<div class="container">
		<form class="form-signin">
			<h1 class="h3 mb-3 font-weight-normal">注册</h1>
			<label for="inputEmail" class="sr-only">请输出电子邮件</label> <input
				type="email" id="inputEmail" class="form-control"
				placeholder="请输入电子邮件" required autofocus>
			<label
				for="inputPassword" class="sr-only">密码</label> <input
				type="text" id="inputPassword" class="form-control"
				placeholder="请输入密码" required>
			<label
				for="inputPassword" class="sr-only">昵称</label> <input
				type="text" id="userName" class="form-control"
				placeholder="请输入昵称" required>
			<label
				for="inputPassword" class="sr-only">手机号</label> <input
				type="text" id="phoneNumber" class="form-control"
				placeholder="请输入手机号" required>
			<button id="register_button" class="btn btn-lg btn-primary btn-block" type="button">注册</button>
			<div style="float: left"><a href="login.jsp" style="color: #005cbf">去登陆>></a></div>
			<p class="mt-5 mb-3 text-muted">&copy; 2017-2021</p>
		</form>
	</div>
</body>