<head>
<jsp:directive.include file="/WEB-INF/jsp/prelude/include-head-meta.jspf" />
	<title>My Home Page</title>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-org.example.user-select: none;
        -moz-org.example.user-select: none;
        -ms-org.example.user-select: none;
        org.example.user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
</head>
<body>
	<div class="container">
		<form class="form-signin" action="${contextPath}/login">
			<h1 class="h3 mb-3 font-weight-normal">登录</h1>
			<label for="inputEmail" class="sr-only">请输出电子邮件</label> <input name="email"
				type="email" id="inputEmail" class="form-control"
				placeholder="请输入电子邮件" required autofocus>
			<label
				for="inputPassword" class="sr-only">Password</label> <input name="password"
				type="password" id="inputPassword" class="form-control"
				placeholder="请输入密码" required>
			<span style="color: red">${errorMsg}</span>
			<div class="checkbox mb-3">
				<label> <input type="checkbox" value="remember-me">
					Remember me
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
			<div>
				<a href="/register.jsp" style="float: left; color: #1b1e21">去注册>></a>
			</div>
			<p class="mt-5 mb-3 text-muted">&copy; 2017-2021</p>
		</form>
	</div>