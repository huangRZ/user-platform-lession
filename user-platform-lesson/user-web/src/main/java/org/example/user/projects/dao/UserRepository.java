package org.example.user.projects.dao;


import org.example.user.projects.domain.User;

/**
 * 写点什么？
 *
 * @author hrz
 * @since 2021/3/3 0003
 */
public class UserRepository {

    /**
     * 根据用户名和密码查询用户
     */
    private static final String SELECT_ONE_SQL = "select * from users where email = ? and password = ?";

    private static final String INSERT_ONE_SQL = "insert into users(name, password, email, phoneNumber) values(?, ?, ?, ?)";

    public User selectOne(String email, String password) throws Throwable {
        return DbUtil.executeQuery(User.class, SELECT_ONE_SQL, email, password);
    }

    public boolean insertOne(User user) throws Throwable {
        return DbUtil.executeUpdate(INSERT_ONE_SQL, user.getName(), user.getPassword(), user.getEmail(), user.getPhoneNumber());
    }

}
