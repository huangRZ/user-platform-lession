package org.example.user.projects.service;


import org.example.user.context.ComponentContext;
import org.example.user.projects.dao.UserRepository;
import org.example.user.projects.domain.User;

import javax.naming.NamingException;

/**
 * 写点什么？
 *
 * @author hrz
 * @since 2021/3/4 0004
 */
public class UserService {

    private UserRepository userRepository;

    public UserService() throws NamingException {
        this.userRepository = ComponentContext.getInstance().lookComponent("bean/UserRepository");
    }

    public User findByEmail(String email, String password) throws Throwable {
        return userRepository.selectOne(email, password);
    }

    public boolean register(User user) throws Throwable {
        return userRepository.insertOne(user);
    }
}
